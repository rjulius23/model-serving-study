import click
from pathlib import Path
from tensorflow.keras.applications import EfficientNetB0
from tensorflow.python.saved_model import tag_constants
import tensorflow as tf


def get_graph_def_from_file(graph_filepath):
    """Get graph from pb file."""
    with tf.compat.v1.gfile.GFile(str(graph_filepath), "rb") as f:
        graph_def = tf.compat.v1.GraphDef()
        graph_def.ParseFromString(f.read())
        return graph_def


def get_graph_def_from_saved_model(saved_model_dir: Path):
    """Get graph from saved_model."""
    with tf.compat.v1.Session() as session:
        meta_graph_def = tf.compat.v1.saved_model.loader.load(
            sess=session, tags=[tag_constants.SERVING], export_dir=str(saved_model_dir),
        )
    return meta_graph_def.graph_def


def describe_graph(graph_def, show_nodes=False):
    """Describe graph parameters."""
    print(
        "Input Feature Nodes: {}".format(
            [node.name for node in graph_def.node if node.op == "Placeholder"]
        )
    )
    print("")
    print(
        "Unused Nodes: {}".format(
            [node.name for node in graph_def.node if "unused" in node.name]
        )
    )
    print("")
    print(
        "Output Nodes: {}".format(
            [
                node.name
                for node in graph_def.node
                if ("predictions" in node.name or "softmax" in node.name)
            ]
        )
    )
    print("")
    print(
        "Quantization Nodes: {}".format(
            [node.name for node in graph_def.node if "quant" in node.name]
        )
    )
    print("")
    print(
        "Constant Count: {}".format(
            len([node for node in graph_def.node if node.op == "Const"])
        )
    )
    print("")
    print(
        "Variable Count: {}".format(
            len([node for node in graph_def.node if "Variable" in node.op])
        )
    )
    print("")
    print(
        "Identity Count: {}".format(
            len([node for node in graph_def.node if node.op == "Identity"])
        )
    )
    print("", "Total nodes: {}".format(len(graph_def.node)), "")

    if show_nodes == True:
        for node in graph_def.node:
            print("Op:{} - Name: {}".format(node.op, node.name))


def get_size(model_dir, model_file="saved_model.pb"):
    """Get size of the model."""
    model_file_path = model_dir / model_file
    print(model_file_path, "")
    pb_size = model_file_path.stat().st_size
    variables_size = 0
    if (model_dir / "variables/variables.data-00000-of-00001").exists():
        variables_size = (
            (model_dir / "variables/variables.data-00000-of-00001").stat().st_size
        )
        variables_size += (model_dir / "variables/variables.index").stat().st_size
    print("Model size: {} KB".format(round(pb_size / (1024.0), 3)))
    print("Variables size: {} KB".format(round(variables_size / (1024.0), 3)))
    print("Total Size: {} KB".format(round((pb_size + variables_size) / (1024.0), 3)))


@click.command()
@click.option("--version", default=1, help="Version of the model.")
@click.option("--describe", is_flag=True, help="Describe graph of the saved model.")
@click.option("--size", is_flag=True, help="Get model size.")
@click.option("--optimize", is_flag=True, help="Freeze graph")
def optimize_and_save(version, describe, size, optimize):
    """Optimize and save efficientNet model."""

    model = EfficientNetB0(weights="imagenet")
    export_path = Path(f"./model/efficientNet_v{version}/{version}/")

    if not export_path.exists():
        print("Path is not existing...")
        export_path.mkdir(parents=True, exist_ok=True)

        tf.keras.models.save_model(
            model,
            str(export_path),
            overwrite=True,
            include_optimizer=True,
            save_format=None,
            signatures=None,
            options=None,
        )

    if describe:
        graph = get_graph_def_from_saved_model(export_path)
        describe_graph(graph_def=graph)

    if size:
        get_size(export_path)


if __name__ == "__main__":
    optimize_and_save()
