#!/bin/bash

docker build -f ./docker/Dockerfile.base -t serving_base ../.
docker build -f ./docker/Dockerfile -t serving_app ../.