from io import BytesIO
from grpc import insecure_channel
import numpy as np
from pathlib import Path
import tensorflow as tf
from timeit import default_timer as timer
from tensorflow.keras import layers
from tensorflow.keras.applications.efficientnet import decode_predictions
from tensorflow_serving.apis import predict_pb2

from PIL import Image  # pylint: disable=wrong-import-order


def predict(image, stub, model="efficientNet_v1", timeout=10.0):
    """
    Retrieve a prediction from a TensorFlow model server
    :param image:       a MNIST image represented as a 1x784 array
    :param stub:        stub to communicate with serving server
    :param model: the name of model to use
    :param timeout:     the amount of time to wait for a prediction to complete
    :return 0:          the integer predicted in the MNIST image
    :return 1:          the confidence scores for all classes
    :return 2:          the version number of the model handling the request
    """

    # build request
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model
    request.model_spec.signature_name = "serving_default"
    request.inputs["input_1"].CopyFrom(
        tf.make_tensor_proto(
            image,
            shape=[image.size[1], image.size[0], 3],
            verify_shape=True,
            dtype=float,
        )
    )

    # retrieve results
    start = timer()
    serving_resp = stub.Predict(request, timeout)
    end = timer()

    pred_duration = (end - start) * 1000.0
    results = {}

    start = timer()
    for output in serving_resp.outputs.keys():
        results[output] = tf.make_ndarray(serving_resp.outputs[output])

    # Return only the top1 the highes probability prediction
    decoded = decode_predictions(results["predictions"], top=1)[0]

    response = []
    for i, val in enumerate(decoded):
        pred = {}
        pred["class"] = val[1]
        pred["probability"] = float(val[2])
        response.append(pred)
    end = timer()
    postproc_duration = (end - start) * 1000.0

    return pred_duration, postproc_duration, response


def preprocess_image(image: Image.Image) -> Image.Image:
    """Resize image to 512x512 and scale it between -1,1"""
    # Switch to 224x224 as EfficientB0 is better suited for that size.
    IMG_SIZE = 224

    resize_and_rescale = tf.keras.Sequential(
        [
            layers.experimental.preprocessing.Resizing(IMG_SIZE, IMG_SIZE),
            layers.experimental.preprocessing.Rescaling(scale=1.0 / 127.5, offset=-1),
        ]
    )

    img = tf.keras.preprocessing.image.img_to_array(image)
    image = tf.keras.preprocessing.image.array_to_img(resize_and_rescale(img))
    return image


def read_imagefile(file) -> Image.Image:
    image = Image.open(BytesIO(file))

    return preprocess_image(image)
