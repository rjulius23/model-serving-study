import uvicorn
import grpc
from grpc import channel_ready_future, insecure_channel
from timeit import default_timer as timer
from datetime import datetime
from fastapi import FastAPI, File, UploadFile
from influxdb import InfluxDBClient
import os
from tensorflow_serving.apis import prediction_service_pb2_grpc
from starlette.responses import RedirectResponse
from serving_app.service.ml_tools import predict, read_imagefile

app = FastAPI()


def init_db() -> InfluxDBClient:
    """Initialize InfluxDB and return client."""
    client = InfluxDBClient("influxdb", 8086, "admin", "admin123!", "prediction_stats")
    dbs = client.get_list_database()
    if "prediction_stats" not in dbs:
        client.create_database("prediction_stats")
    return client


@app.get("/", include_in_schema=False)
async def index():
    return RedirectResponse(url="/docs")


@app.post("/predict/image/<version>")
async def predict_api(file: UploadFile = File(...), version: str = "efficientNet_v1"):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg")
    if not extension:
        return "Image must be jpg or png format!"

    # Load and preprocess image
    start = timer()
    image = read_imagefile(await file.read())
    end = timer()

    preproc_duration = (end - start) * 1000.0

    client = init_db()

    SERVER_HOST = os.getenv("TFLOW_HOST", "tensorflow-serving")
    SERVER_PORT = os.getenv("TFLOW_PORT", 8500)
    start = timer()
    # initialize to server connection
    channel = insecure_channel(f"{SERVER_HOST}:{SERVER_PORT}")
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
    end = timer()
    connect_duration = (end - start) * 1000.0

    pred_duration, postproc_duration, prediction = predict(
        image, stub=stub, model=version
    )

    # Store prediction time in DB
    # pred_duration = (end - start) * 1000.0
    json_body = [
        {
            "measurement": "serving_measurements",
            "tags": {"model": version},
            "time": datetime.now().isoformat(),
            "fields": {
                "prediction_time": pred_duration,
                "preprocessing_time": preproc_duration,
                "postprocessing_time": postproc_duration,
                "connect_time": connect_duration,
            },
        }
    ]
    client.write_points(json_body)

    return prediction


if __name__ == "__main__":
    uvicorn.run(app, debug=True)
