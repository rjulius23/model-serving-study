# Model Serving Study

In this repo, I am studying possible ways of delivering machine learning models to production.

## **Minimum Value Product**

Deploy ML model using tensorflow serving + FastAPI.

### **Serving Framework**

#### **Benchmark Serving Framework and API**

As the selected model is a TF model the simplest solution is to use tensorflow/serving docker image keeping deployment in mind too.

[TensorFlow/Serving](https://github.com/tensorflow/serving)

For benchmarking I have forked the a benchmarking repo for tensor flow serving and included it as a subrepo

[Original repo](https://github.com/anthony0727/tensorflow-serving-benchmark)
[Forked repo](https://github.com/rjulius23/tensorflow-serving-benchmark)

Changes:

1. Updated python and tensorflow versions.
2. Adapted code to newer versions.
3. Added fastapi to benchmarks.

**Benchmarks**

1. Check out repo
   ```
   git clone https://gitlab.com/rjulius23/model-serving-study.git study
   cd study
   git submodule update --init --recursive
   ```
2. Go to benchmarking folder
   ```
   cd mvp/benchmark/tensorflow-serving-benchmark
   ```
3. Run benchmarks
   1. ```docker-compose run fastapi-benchmark```
   2. ```docker-compose run tornado-benchmark```
   3. ```docker-compose run wsgi-benchmark```
   4. ```docker-compose run grpc-benchmark```

4. Make sure the environment is cleaned up
    ```docker-compose down```

**Results**

WSGI - FastAPI:

```
Server Software:        uvicorn
Server Hostname:        fastapi-client
Server Port:            8002

Document Path:          /prediction
Document Length:        31 bytes

Concurrency Level:      10
Time taken for tests:   2.417 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      1910000 bytes
HTML transferred:       310000 bytes
Requests per second:    4137.41 [#/sec] (mean)
Time per request:       2.417 [ms] (mean)
Time per request:       0.242 [ms] (mean, across all concurrent requests)
Transfer rate:          771.72 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:     1    2   1.1      2      11
Waiting:        0    2   0.9      2      11
Total:          1    2   1.1      2      12

Percentage of the requests served within a certain time (ms)
  50%      2
  66%      3
  75%      3
  80%      3
  90%      4
  95%      4
  98%      5
  99%      6
 100%     12 (longest request)
 ```

WSGI - Falcon:

```
Server Software:        gunicorn/19.7.1
Server Hostname:        wsgi-client
Server Port:            8000

Document Path:          /prediction
Document Length:        141 bytes

Concurrency Level:      10
Time taken for tests:   3.137 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      2440000 bytes
HTML transferred:       1410000 bytes
Requests per second:    3188.21 [#/sec] (mean)
Time per request:       3.137 [ms] (mean)
Time per request:       0.314 [ms] (mean, across all concurrent requests)
Transfer rate:          759.69 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       4
Processing:     0    2  10.7      2     902
Waiting:        0    2  10.7      2     901
Total:          0    2  10.7      2     902

Percentage of the requests served within a certain time (ms)
  50%      2
  66%      2
  75%      2
  80%      2
  90%      3
  95%      3
  98%      6
  99%      9
 100%    902 (longest request)
 ```

 Tornado:

 ```
Server Software:        TornadoServer/6.1
Server Hostname:        tornado-client
Server Port:            8001

Document Path:          /prediction
Document Length:        93 bytes

Concurrency Level:      10
Time taken for tests:   4.154 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      2550000 bytes
HTML transferred:       930000 bytes
Requests per second:    2407.06 [#/sec] (mean)
Time per request:       4.154 [ms] (mean)
Time per request:       0.415 [ms] (mean, across all concurrent requests)
Transfer rate:          599.41 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       4
Processing:     1    4   2.4      4      21
Waiting:        0    4   2.4      3      21
Total:          1    4   2.4      4      21

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      5
  75%      5
  80%      6
  90%      7
  95%      9
  98%     11
  99%     12
 100%     21 (longest request)
 ```

 Native gRPC

 ```
 10000 requests (10 max concurrent)
 3290.316517431216 requests/second
 ```

Based on above results it is clear that the FastAPI with uvicorn is the best option for the Serving API.

#### **Serving Server**

It supports gRPC and REST API as well, in my case I have chosen gRPC due to simpler management of transferring binaries across the wire.

For the deployment of the serving container, docker-compose is used, so it can be deployed easily with all the necessary configuration in place.

#### **Serving API**

The serving API has been built using FastAPI, other alternatives were Tornado or Falcon, however FastAPI is the fastest (lowest latency and less overhead) among all. Flask and Django could have been benchmarked too, but given their use cases, it is less likely that they perform better.

Endpoints:

`GET /` --> SWAGGER UI for ease of use

`POST /predict/image` --> Upload image and receive class prediction

### **How to Run**


1. Set repo path:

    ```
    git clone https://gitlab.com/rjulius23/model-serving-study.git
    export REPO_PATH=abs/path/to/repo
    ```
2. Initiate model storage:

    ```
    cd $REPO_PATH/mvp/deploy
    python3 describe_and_save.py --version 1
    ```
    **NOTE**: This saves the EfficientNetB0 model under mvp/deploy/model/efficientNet_v1/1 location. It will be mounted to the tf_server later. We can modify the ```describe_and_save.py``` to generate different models in this case an alternative version can be passed as an argument and if the path is also updated, then the ```model_config``` file needs to be adapted as well.

3. Build webserver container:

    ```
    cd $REPO_PATH/mvp/deploy
    chmod +x build_docker.sh
    ./build_docker.sh
    ```
4. Initiate environment
    ```
    cd $REPO_PATH/mvp/deploy
    docker-compose up -d
    ```

    **NOTE**: The tensorflow_server and our webserver should be up and running, listening on http://0.0.0.0:8888

5. Open the webserver listen address in a Browser.
6. Select the /predict/image endpoint and hit "Try it Out!"
7. Upload an image and execute.
8. Result shoudl be something like:
   
    ``` 	
    Response body
    [
        {
            "class": "German_shepherd",
            "probability": 0.8935041427612305
        }
    ]
    ```

## Monitoring

### **TensorBoard**

[Follow the ducomentation for monitoring TensorFlow Serving](https://www.tensorflow.org/tfx/serving/tensorboard)

In the provided ```mvp/deploy/docker-compose.yml``` there is also a TensorBoard instance.

### **Monitoring with Prometheus built-in TensorFlow/Serving**

In the provided ```mvp/deploy/docker-compose.yml``` there are a prometheus and grafana instance to monitor the serving server with its built in metrics.

To enable monitoring the monitoring_config file has to be provided during startup of the serving server (see docker-compose.yml).

### **Integrate InfluxDB into Serving API**

I have integrated InfluxDB into the Serving API to accurately monitor the actual prediction time required.

For visualization grafana can be used as well

The dashboards can be configured using the InfluxDB or the Prometheus Datasources. In InfluxDb the database is defined and set up in the ```init_db()``` function. The database is prediction_stats, the example content is like the following:
```
        {
            "measurement": "serving_metrics",
            "tags": {"model": version},
            "time": datetime.now().isoformat(),
            "fields": {"prediction_time": duration},
        }
```

### **Visualization of metrics with Grafana**

![Dashboard](mvp/dashboard/pred_stats.PNG?raw=true "Title")

## Optimization

According to:
[TensorFlow Serving Performance Guide](https://www.tensorflow.org/tfx/serving/performance)

We can roughly think about 3 groups of parameters whose configuration determines observed performance: 1) the TensorFlow model 2) the inference requests and 3) the server (hardware & binary).

**Set up the Analysis tool:**

1. Install dependencies
    ```
    cd $REPO_PATH/mvp/deploy
    python3 -m pip install --user --upgrade -r ../dev-requirements.txt
    ```

2. Describe and get size of a model
    ```
    python3 describe_and_save.py --version 1 --describe --size
    ```

3. Also use the built in tool provided by tensorflow
    ```
    saved_model_cli show --dir ./model/efficientNet_v1/1/ --all
    ```
### **TensorFlow Model**

We are using the EfficientNetB0 model, if we do a bit of analysis we can see the following:

Save model output:

```
signature_def['__saved_model_init_op']:
  The given SavedModel SignatureDef contains the following input(s):
  The given SavedModel SignatureDef contains the following output(s):
    outputs['__saved_model_init_op'] tensor_info:
        dtype: DT_INVALID
        shape: unknown_rank
        name: NoOp
  Method name is: 

signature_def['serving_default']:
  The given SavedModel SignatureDef contains the following input(s):
    inputs['input_1'] tensor_info:
        dtype: DT_FLOAT
        shape: (-1, 224, 224, 3)
        name: serving_default_input_1:0
  The given SavedModel SignatureDef contains the following output(s):
    outputs['predictions'] tensor_info:
        dtype: DT_FLOAT
        shape: (-1, 1000)
        name: StatefulPartitionedCall:0
  Method name is: tensorflow/serving/predict
```

Analysis tool output:

```
Input Feature Nodes: ['serving_default_input_1', 'saver_filename']

Unused Nodes: []

Output Nodes: ['predictions/kernel', 'predictions/kernel/Read/ReadVariableOp', 'predictions/bias', 'predictions/bias/Read/ReadVariableOp']

Quantization Nodes: []

Constant Count: 1

Variable Count: 314

Identity Count: 0
 Total nodes: 635 
model/efficientNet_v1/1/saved_model.pb 
Model size: 4270.923 KB
Variables size: 20967.129 KB
Total Size: 25238.052 KB
```

We probably can get away using 224x224 images instead of 512x512 as the input layers is more adapted for that. We can also apply quantization on the model from float32 to float16 or even int8,. However to do this we would transfer the model to tflite format and since TensorFlow 1.9 it is not supported to convert tflite back to pb. Our current serving framework only supports frozen model format. As our target HW is a V100 and 16GB RAM the benefit wouldnt be significant. TFLite is better designed for embedded or portable devices.

**Converting to TensorRT Model**
Using the deployment server with V100 can be an option to convert the model to TensorRT model to match the target architecture of the V100 and have better performance on the deployment server. The disadvantage is that in this case the best performance is only available on the specific type of HW.

### **Serving API and Requests**

We are using Tensorflow's Prediction API, which is highly optimized and fine tuned. Instead of HTTP we decided to use GRPC which should have even better performance attributes. However it is possible to improve the prediction performance by increasing the batch size. At the moment we use single images for predictions, however the target HW with 16GB VRAM can support way more than that. It depends on the workflow if our service takes pictures in batches this could be a good direction to improve performance.

### **The HW**

See above discussed topics related to HW (TensorRT, increased batch size to fully use the VRAM).

## Future Improvements

Deploy solution on one of the Public Cloud providers possible solutions:

1. **Deploy in VM with GPU**
   1. This way it is possible to deploy using a VM flavor that matches the deployment server requirements.
   2. It is not the most scalable solution and requires some infrastructure set up.
   3. Fully production grade.
   
2. **Deploy using ML Service**
   1. ML Services are new products in Cloud Providers repertoir, it is designed for deploying Model as a Code to production ready environments.
   2. We only need to provide the Model as a Code or Model as a Data.
   3. Production grade.
   
3. **Deploy using Serverless**
   1. Fully scalable, no need to worry about infrastructure set up.
   2. Not the most reliable solution and it may not be possible to match the deployment server specifications.
   3. Best for experimentation in Cloud environment.













